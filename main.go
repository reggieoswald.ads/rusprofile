package main

import (
	"rusprofile/internal/config"
	"rusprofile/pkg/integrations"
	"rusprofile/pkg/server"
	"rusprofile/pkg/services"
)

func main() {
	var (
		cfg          = config.Get()
		loggerClient = services.NewLogger()
		rusProfile   = integrations.NewRusProfile(cfg)
	)

	go server.StartHTTPServer(cfg, loggerClient, rusProfile)

	server.StartGRPCServer(cfg, loggerClient, rusProfile)
}
