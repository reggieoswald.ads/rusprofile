package integrations

import (
	"bytes"
	"fmt"
	"net/http"

	"github.com/PuerkitoBio/goquery"
	"github.com/go-resty/resty/v2"
	"github.com/pkg/errors"
	"rusprofile/internal/config"
	"rusprofile/pkg/domain"
)

type rusProfile struct {
	client *resty.Client
}

func NewRusProfile(cfg *config.Config) domain.RusProfile {
	return &rusProfile{
		client: resty.New().SetHostURL(cfg.RusProfileURI),
	}
}

func (r *rusProfile) GetCompany(inn string) (*domain.Company, error) {
	resp, err := r.client.R().Get(fmt.Sprintf("/search?query=%s&search_inactive=0", inn))
	if err != nil {
		return nil, errors.WithStack(err)
	}

	fmt.Println(resp.StatusCode())

	if resp.StatusCode() != http.StatusOK {
		return nil, &domain.ErrRequestFailed{
			Message:    resp.String(),
			StatusCode: resp.StatusCode(),
		}
	}

	return parse(resp.Body())
}

func parse(body []byte) (*domain.Company, error) {
	document, err := goquery.NewDocumentFromReader(bytes.NewReader(body))
	if err != nil {
		return nil, err
	}

	company := new(domain.Company)

	findInDocument(document, company)

	return company, nil
}

func findInDocument(document *goquery.Document, company *domain.Company) {
	document.Find("span#clip_inn").Each(func(_ int, selection *goquery.Selection) {
		company.INN = selection.Text()
	})
	document.Find("span#clip_kpp").Each(func(_ int, selection *goquery.Selection) {
		company.KPP = selection.Text()
	})
	document.Find("div.company-name").Each(func(_ int, selection *goquery.Selection) {
		company.Name = selection.Text()
	})
	document.Find("div.company-row.hidden-parent span.company-info__text").Each(func(_ int, s *goquery.Selection) {
		company.HeadFullName = s.Text()
	})
}
