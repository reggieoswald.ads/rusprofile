package server

import (
	"context"
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"rusprofile/internal/config"
	gateway "rusprofile/pb/gateway"
	"rusprofile/pkg/domain"
)

func StartHTTPServer(cfg *config.Config, loggerClient domain.Logger, rusProfile domain.RusProfile) {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := runtime.NewServeMux()

	if err := gateway.RegisterRusProfileSrvHandlerServer(ctx, mux, New(rusProfile, loggerClient)); err != nil {
		loggerClient.Panic(err, []domain.LogValue{
			{domain.LogMessageFieldName, "failed to register"},
			{"port", cfg.HTTPPort},
		})
	}

	loggerClient.Info("server started", []domain.LogValue{
		{"port", cfg.HTTPPort},
	})
	if err := http.ListenAndServe(":"+cfg.HTTPPort, mux); err != nil {
		loggerClient.Panic(err, []domain.LogValue{
			{domain.LogMessageFieldName, "failed to listen and serve"},
			{"port", cfg.HTTPPort},
		})
	}
}
