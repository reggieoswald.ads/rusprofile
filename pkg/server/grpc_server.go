package server

import (
	"net"

	"google.golang.org/grpc"

	"rusprofile/internal/config"
	"rusprofile/pb"
	"rusprofile/pkg/domain"
)

func StartGRPCServer(cfg *config.Config, loggerClient domain.Logger, rusProfile domain.RusProfile) {
	var (
		err      error
		listener net.Listener

		s = grpc.NewServer()
	)

	if listener, err = net.Listen("tcp", ":"+cfg.GRPCPort); err != nil {
		loggerClient.Panic(err, []domain.LogValue{
			{domain.LogMessageFieldName, "failed to listen"},
			{"port", cfg.GRPCPort},
		})
	}

	pb.RegisterRusProfileSrvServer(s, New(rusProfile, loggerClient))
	loggerClient.Info("grpc server started", []domain.LogValue{
		{"port", cfg.GRPCPort},
	})
	if err = s.Serve(listener); err != nil {
		loggerClient.Panic(err, []domain.LogValue{
			{domain.LogMessageFieldName, "failed to serve"},
		})
	}
}
