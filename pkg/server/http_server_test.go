package server

import (
	"encoding/json"
	"fmt"
	"net/http"
	"testing"

	"github.com/go-resty/resty/v2"
	"github.com/stretchr/testify/require"
	"rusprofile/internal/config"
	"rusprofile/pkg/domain"
	"rusprofile/pkg/integrations"
	"rusprofile/pkg/server/consts"
	"rusprofile/pkg/services"
)

func TestHTTPServer(t *testing.T) {
	var (
		cfg          = config.Get()
		loggerClient = services.NewLogger()
		rusProfile   = integrations.NewRusProfile(cfg)
	)

	go StartHTTPServer(cfg, loggerClient, rusProfile)

	client := resty.New().SetHostURL(fmt.Sprintf("http://localhost:%s", cfg.HTTPPort))

	type request struct {
		Inn string `json:"inn"`
	}

	resp, err := client.R().SetBody(request{Inn: consts.CompanyInn}).Post("/company")
	require.Nil(t, err)
	require.Equal(t, http.StatusOK, resp.StatusCode())

	data := new(domain.Company)

	err = json.Unmarshal(resp.Body(), data)
	require.Nil(t, err)

	require.Equal(t, consts.CompanyInn, data.INN)
	require.Equal(t, consts.CompanyKpp, data.KPP)
	require.Equal(t, consts.CompanyName, data.Name)
	require.Equal(t, consts.CompanyHeadFullName, data.HeadFullName)
}
