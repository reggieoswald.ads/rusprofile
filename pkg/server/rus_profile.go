package server

import (
	"context"

	"github.com/pkg/errors"
	"rusprofile/pb"
	"rusprofile/pkg/domain"
)

type server struct {
	pb.UnimplementedRusProfileSrvServer

	logger     domain.Logger
	rusProfile domain.RusProfile
}

func New(rusProfile domain.RusProfile, logger domain.Logger) pb.RusProfileSrvServer {
	return &server{
		logger:     logger,
		rusProfile: rusProfile,
	}
}

func (s *server) GetCompany(_ context.Context, request *pb.GetCompanyRequest) (response *pb.GetCompanyResponse, err error) {
	var company *domain.Company

	if company, err = s.rusProfile.GetCompany(request.Inn); err != nil {
		s.logger.Error(err, nil)
	}

	if company == nil {
		return nil, errors.Errorf("no company for inn: %s", request.Inn)
	}

	return &pb.GetCompanyResponse{
		Inn:          company.INN,
		Kpp:          company.KPP,
		Name:         company.Name,
		HeadFullName: company.HeadFullName,
	}, nil
}
