package server

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
	"rusprofile/internal/config"
	"rusprofile/pb"
	"rusprofile/pkg/integrations"
	"rusprofile/pkg/server/consts"
	"rusprofile/pkg/services"
	"rusprofile/utils"
)

func TestGRPCServer(t *testing.T) {
	var (
		ctx          = context.Background()
		cfg          = config.Get()
		loggerClient = services.NewLogger()
		rusProfile   = integrations.NewRusProfile(cfg)
	)

	go StartGRPCServer(cfg, loggerClient, rusProfile)

	client := pb.NewRusProfileSrvClient(utils.GetGRPCConnection(fmt.Sprintf("localhost:%s", cfg.GRPCPort)))

	company, err := client.GetCompany(ctx, &pb.GetCompanyRequest{Inn: consts.CompanyInn})
	require.Nil(t, err)

	require.Equal(t, consts.CompanyInn, company.Inn)
	require.Equal(t, consts.CompanyKpp, company.Kpp)
	require.Equal(t, consts.CompanyName, company.Name)
	require.Equal(t, consts.CompanyHeadFullName, company.HeadFullName)
}
