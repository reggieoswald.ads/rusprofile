package services

import (
	"fmt"
	"github.com/rs/zerolog"
	"os"
	"rusprofile/pkg/domain"
	"strings"
	"time"
)

type logger struct {
	zero zerolog.Logger
}

func NewLogger() domain.Logger {
	return &logger{
		zero: zerolog.New(zerolog.ConsoleWriter{
			Out: os.Stdout,
			FormatLevel: func(i interface{}) string {
				return strings.ToUpper(fmt.Sprintf("| %-6s|", i))
			},
			FormatTimestamp: func(i interface{}) string {
				return time.Now().Format(time.RFC3339)
			},
		}),
	}
}

func (l *logger) Info(message string, values []domain.LogValue) {
	event := l.zero.Info()

	for _, value := range values {
		event = event.Interface(value.Key, value.Payload)
	}
	event.Msg(message)
}

func (l *logger) Error(err error, values []domain.LogValue) {
	event := l.zero.Error().Err(err)

	msgPassed := l.iFace(values, event)
	event.Msg(msgPassed)
}

func (l *logger) Panic(err error, values []domain.LogValue) {
	event := l.zero.Panic().Err(err)

	event.Msg(l.iFace(values, event))
}

func (l *logger) iFace(values []domain.LogValue, event *zerolog.Event) (msgPassed string) {
	for _, value := range values {
		if value.Key == zerolog.MessageFieldName {
			if val, ok := value.Payload.(string); ok {
				msgPassed = val
			}
		}
		event = event.Interface(value.Key, value.Payload)
	}
	return msgPassed
}
