package domain

type RusProfile interface {
	GetCompany(inn string) (*Company, error)
}

type Company struct {
	INN          string // идентификационный номер налогоплательщика
	KPP          string // код причины постановки на учёт
	Name         string // название компании
	HeadFullName string // ФИО руководителя
}

type ErrRequestFailed struct {
	Message    string
	StatusCode int
}

func (e *ErrRequestFailed) Error() string {
	return "request_failed"
}
