package domain

import "github.com/rs/zerolog"

type Logger interface {
	Info(message string, values []LogValue)
	Error(err error, values []LogValue)
	Panic(err error, values []LogValue)
}

type LogValue struct {
	Key     string
	Payload interface{}
}

var LogMessageFieldName = zerolog.MessageFieldName
