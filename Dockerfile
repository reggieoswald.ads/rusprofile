FROM golang:alpine AS build-stage

RUN export GOPATH=$HOME/go
RUN export GOBIN=$(go env GOPATH)/bin
RUN export PATH=$PATH:$GOPATH
RUN export PATH=$PATH:$GOBIN

WORKDIR $GOPATH/src/rusprofile/

COPY internal/ internal/
COPY pb/ pb/
COPY pkg/ pkg/
COPY utils/ utils/
COPY main.go/ ./
COPY go.mod/ ./
RUN cd pb && go mod tidy && cd ../
RUN go mod tidy
RUN go build -o rusprofile . && cp rusprofile /
RUN rm rusprofile

FROM alpine:latest
RUN apk --no-cache add ca-certificates
ENV RUS_PROFILE_HTTP_PORT="50059"
ENV RUS_PROFILE_GRPC_PORT="50060"
ENV RUS_PROFILE_URI="https://www.rusprofile.ru"

WORKDIR /root/
COPY --from=build-stage rusprofile .
ENTRYPOINT ["./rusprofile"]