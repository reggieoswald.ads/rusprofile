.SILENT:

clean_proto: ## clean proto files
	cd pb && rm -rf *.pb.go *.pb.gw.go *.swagger.json google gateway
.PHONY: clean_proto

proto: ## generate proto
	cd pb && mkdir -p google/api gateway \
    && curl https://raw.githubusercontent.com/googleapis/googleapis/master/google/api/annotations.proto > google/api/annotations.proto \
    && curl https://raw.githubusercontent.com/googleapis/googleapis/master/google/api/http.proto > google/api/http.proto \
    && protoc -I . --grpc-gateway_out ./gateway \
        --swagger_out=. \
        --grpc-gateway_opt logtostderr=true \
        --grpc-gateway_opt paths=source_relative \
        --grpc-gateway_opt standalone=true \
        *.proto \
   	&& protoc --go_out=. --go_opt=paths=source_relative -I=. --go-grpc_out=. --go-grpc_opt=paths=source_relative *.proto \
   	&& go mod tidy
.PHONY: proto

build: ## build executable file
	go get google.golang.org/genproto/googleapis/api/annotations@v0.0.0-20210903162649-d08c68adba83 \
	&& go mod tidy \
	&& cd cmd/api && go build
.PHONY: build

run: ## run server
	cd cmd/api && ./api
.PHONY: run

test: ## run local tests
	cd pkg/server && go test
.PHONY: test

docker: ## build && run docker container
	make proto &&  /bin/sh ./rusprofile.sh
.PHONY: test

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
.PHONY: help

.DEFAULT_GOAL := help