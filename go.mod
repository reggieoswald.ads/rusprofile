module rusprofile

go 1.16

require (
	github.com/PuerkitoBio/goquery v1.7.1
	github.com/go-resty/resty/v2 v2.6.0
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.6.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/pkg/errors v0.9.1
	github.com/rs/zerolog v1.25.0
	github.com/stretchr/testify v1.7.0
	google.golang.org/grpc v1.41.0
	gopkg.in/yaml.v2 v2.4.0
	rusprofile/pb v0.0.0-00010101000000-000000000000
)

replace rusprofile/pb => ./pb
