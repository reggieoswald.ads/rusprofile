SERVER_ADDR:=localhost:50060

invoke.GetCompany:
	grpcurl -plaintext -proto=./rusprofile.proto \
		-d '{ \
				"inn": "7721085846" \
			}' \
		${SERVER_ADDR} pb.RusProfileSrv/GetCompany