module rusprofile/pb

go 1.16

require (
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.6.0
	google.golang.org/genproto v0.0.0-20211001223012-bfb93cce50d9
	google.golang.org/grpc v1.41.0
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.1.0
	google.golang.org/protobuf v1.27.1
)
