package config

import (
	"os"
	"path"
	"runtime"

	"gopkg.in/yaml.v2"

	"github.com/kelseyhightower/envconfig"
	"github.com/pkg/errors"
)

type Config struct {
	HTTPPort      string `yaml:"http_port" envconfig:"RUS_PROFILE_HTTP_PORT"`
	GRPCPort      string `yaml:"grpc_port" envconfig:"RUS_PROFILE_GRPC_PORT"`
	RusProfileURI string `yaml:"rus_profile_uri" envconfig:"RUS_PROFILE_URI"`
}

func processError(err error) {
	panic(err)
}

func readFile(cfg *Config) {
	_, filename, _, ok := runtime.Caller(1)
	if !ok {
		processError(errors.New("unable to get config file directory"))
	}

	file, _ := os.Open(path.Join(path.Dir(filename), "/config.yaml"))
	if file == nil {
		return
	}
	defer func() {
		if err := file.Close(); err != nil {
			processError(err)
		}
	}()

	decoder := yaml.NewDecoder(file)
	err := decoder.Decode(cfg)
	if err != nil {
		processError(err)
	}
}

func readEnv(cfg *Config) {
	err := envconfig.Process("", cfg)
	if err != nil {
		processError(err)
	}
}

func Get() *Config {
	var cfg Config
	readFile(&cfg)
	readEnv(&cfg)

	if cfg.HTTPPort == "" {
		processError(errors.New("http port is not set in config"))
	}
	if cfg.GRPCPort == "" {
		processError(errors.New("gRPC port is not set in config"))
	}
	if cfg.RusProfileURI == "" {
		processError(errors.New("rusprofile uri is not set in config"))
	}

	return &cfg
}
