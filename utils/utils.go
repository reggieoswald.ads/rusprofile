package utils

import (
	"google.golang.org/grpc"
	"log"
)

func GetGRPCConnection(host string) *grpc.ClientConn {
	conn, err := grpc.Dial(host, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("can not connect to %v", err)
	}

	return conn
}
