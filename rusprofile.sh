#!/bin/sh
docker build -t rusprofile -f "$GOPATH"/src/rusprofile/Dockerfile "$GOPATH"/src/rusprofile/ && \
docker run -d -p 50059:50059 -p 50060:50060 \
  --restart unless-stopped \
  --name rusprofile rusprofile