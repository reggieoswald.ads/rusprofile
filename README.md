## RUSPROFILE is a service for a parsing info about company by inn 

### Make commands
    $ make

### Local tests
    $ make proto 
    $ make test

### Docker
    $ make docker

### Test docker
[gRPC](pb/invoke.mk) [HTTP](pb/req.http)


### Swagger documentation
[swagger.json](pb/rusprofile.swagger.json) (use `make proto` first)